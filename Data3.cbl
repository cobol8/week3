       IDENTIFICATION DIVISION. 
       PROGRAM-ID. DATA3.
       AUTHOR. ARTHORN.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  SURNAME           PIC   X(8) VALUE "COUGHLAN".
       01  SALE-PRICE        PIC   9(4)V99.
       01  NUM-OF-EMPLOYEES  PIC   999V99.
       01  SALARY            PIC   9999V99.
       01  COUNTY-NAME       PIC   X(9).
       PROCEDURE DIVISION.
       Begin.
           DISPLAY "1 " SURNAME
           MOVE "SMITH"   TO SURNAME 
           DISPLAY "2 " SURNAME 
           MOVE "FITZWILLIAM"   TO SURNAME 
           DISPLAY "3 " SURNAME  
           .
           DISPLAY ""
           DISPLAY "1 " SALE-PRICE 
           MOVE ZERO   TO   SALE-PRICE 
           DISPLAY "2 "   SALE-PRICE 
           MOVE 25.5   TO SALE-PRICE 
           DISPLAY "3 " SALE-PRICE 
      *    01  SALE-PRICE  PIC   9(4)V99. VALUE 7.553
           MOVE 7.553  TO SALE-PRICE 
           DISPLAY "4 " SALE-PRICE
      *    01  SALE-PRICE  PIC   9(4)V99. VALUE 3425.15
           MOVE 93425.1533  TO SALE-PRICE 
           DISPLAY "5 " SALE-PRICE
      *    01  SALE-PRICE  PIC   9(4)V99. VALUE 0128.00
           MOVE 128  TO SALE-PRICE 
           DISPLAY "6 " SALE-PRICE
           .
           DISPLAY ""
      *    01  NUM-OF-EMPLOYEES  PIC   999V99.
           DISPLAY NUM-OF-EMPLOYEES 
      *    01  NUM-OF-EMPLOYEES  PIC   999V99. VALUE 012.40
           MOVE 12.4   TO NUM-OF-EMPLOYEES 
           DISPLAY "1 " NUM-OF-EMPLOYEES 
      *    01  NUM-OF-EMPLOYEES  PIC   999V99. VALUE 745.00
           MOVE 6745   TO NUM-OF-EMPLOYEES 
           DISPLAY "2 " NUM-OF-EMPLOYEES 
           .
           DISPLAY ""
      *    01  SALARY            PIC   9999V99.
           DISPLAY SALARY 
      *    01  SALARY            PIC   9999V99. VALUE 0745.00
           MOVE NUM-OF-EMPLOYEES TO SALARY 
           DISPLAY SALARY 
           .
           DISPLAY ""
      *    01  COUNTY-NAME       PIC   X(9).
           MOVE "GALWAY"  TO COUNTY-NAME 
           DISPLAY "1 " COUNTY-NAME 
           MOVE ALL "@"   TO COUNTY-NAME 
           DISPLAY  "2 " COUNTY-NAME 
           . 
